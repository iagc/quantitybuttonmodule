define([
   'ko',
   'uiComponent'
], function (ko, Component) {
   'use strict';
   return Component.extend({
       initialize: function () {
           //initialize parent Component
           this._super();
           this.qty = ko.observable(this.defaultQty);
           this.incrementQty = ko.observable(this.defaultQty);
       },
       decreaseQty: function() {
           var newQty = this.qty() - this.incrementQty();
           if (newQty < this.incrementQty()) 
           {
               newQty = this.incrementQty();
           }
           this.qty(newQty);
       },
       increaseQty: function() {
           var newQty = this.qty() + this.incrementQty();
           this.qty(newQty);
       }
   });
});
